package br.com.itau.AgendaTelefonica;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		System.out.println("Bem Vindo a sua Agenda. Favor informar seu Nome:");
		
		boolean novoContato = true;
		Scanner scanner = new Scanner(System.in);
		Agenda agenda = new Agenda(scanner.nextLine());
		Long tel;
		
		try {
			while (novoContato) {
				Contato contato = new Contato();
				System.out.println("Nome do Contato:");
				
				String nome = scanner.nextLine();
				System.out.println("Numero de Telefone do Contato:");
				String telefone  = scanner.nextLine();
				
				tel = Long.parseLong(telefone);
				
				contato.nome = nome;
				contato.telefone = tel;
				agenda.adicionarContato(contato);
				
				System.out.println("Sucesso. Deseja Adicionar novo Contato (S / N)");
				
				novoContato = scanner.nextLine().equalsIgnoreCase("S");
			}
			
			Interface.Imprimir(agenda.toString());
		}catch (Exception e) {
			System.out.println("Telefone Inválido.Abortado");
			main(args);
			return;
		}
		finally {
			scanner.close();
		}
	}

}
