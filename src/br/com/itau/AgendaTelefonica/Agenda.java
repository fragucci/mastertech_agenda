package br.com.itau.AgendaTelefonica;

import java.util.ArrayList;

public class Agenda {
	private String propietario;
	private ArrayList<Contato> contatos = new ArrayList<Contato>();
	
	Agenda (String propietario){
		this.propietario = propietario;
	}
	
	public void adicionarContato(Contato contato) {
		
		this.contatos.add(contato);
	
	}

	@Override
	public String toString() {
		String texto = "Agenda de " + propietario + "\n\t";
		texto += "Seus Contatos:" + "\t\t";
		texto += contatos;
		
		//return texto.replace("[", "").replace("]", "").replace(",","");
		return texto;
			
	}
	
}
